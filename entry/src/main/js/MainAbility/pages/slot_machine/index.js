import prompt from '@system.prompt';
export default {
    data: {
        spinDisabled: false,
        result: [] // 中奖池
    },

    // 开始
    start() {
        const { spinDisabled } = this
        // 点击开始后不可点击
        if (spinDisabled) return false
        // 随机设置奖项 该数据应该从后台接口获取
        let result = []
        for (var i = 0; i < 3; i++) {
            result.push(Math.floor(Math.random() * 6 + 1))
        }
        this.spinDisabled = true;
        this.result = result;
        // 触发组件开始方法
        this.$child('sol-slot-machine').start();
    },
    // 结束
    doFinish() {
        prompt.showToast({
            message: "恭喜你获奖了",
            duration: 2000,
        });
        this.spinDisabled=false;
    },
}
