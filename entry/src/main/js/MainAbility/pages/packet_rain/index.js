export default {
    data: {
        visible: false
    },
    onInit() {
        this.visible = true;
        this.createSpeed = 5;
        this.time = 15;
        this.readyTime = 3;
        this.min = 0;
        this.max = 10;
    },
    // 结束
    success() {
        this.visible = false; //  隐藏界面
    },
}
