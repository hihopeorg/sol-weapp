import router from '@system.router';
export default{
    data: {
        endDate: "2021/09/13 16:00:00",
        components: [
            {
                title: "营销组件",
                children: [
                    {
                        id: "big-wheel",
                        url: "pages/big_wheel/index",
                        name: "大转盘动画",
                        show: true,
                        imgUrl:"common/images/turntable.png",
                    },
                    {
                        id: "packet-rain",
                        url: "pages/packet_rain/index",
                        name: "红包雨动画",
                        show: false,
                        imgUrl:"common/images/redpacket.png",
                    },
                    {
                        id: "grid-card",
                        url: "pages/grid_card/index",
                        name: "九宫格翻牌动画",
                        show: true,
                        imgUrl:"common/images/nine.png",
                    },
                    {
                        id: "slot-machine",
                        url: "pages/slot_machine/index",
                        name: "老虎机动画",
                        show: false,
                        imgUrl:"common/images/machine.png",
                    },
                ],
            },
            {
                title: "业务组件",
                children: [
                    {
                        id: "scoll-nav",
                        url: "pages/scoll_nav/index",
                        name: "滚动导航条",
                        show: false,
                        imgUrl:"common/images/navigation.png",
                    },
                    {
                        id: "share-sheet",
                        url: "pages/painter/index",
                        name: "微信分享组件(转发好友/分享图)",
                        show: false,
                        imgUrl:"common/images/share.png",
                    },
                    {
                        id: "xuanfu",
                        url: "pages/float_icon/index",
                        name: "悬浮按钮",
                        show: true,
                        imgUrl:"common/images/float.png",
                    },
                ],
            },
            {
                title: "简单组件",
                children: [
                    {
                        id: "shuzi",
                        url: "pages/animate_number/index",
                        name: "滚动数字",
                        show: true,
                        imgUrl:"common/images/scroll.png",
                    },
                ],
            },
        ],
        icons: ["home", "cart"],
    },
    onReady() {
//        var myDate = new Date().getTime();
//        var endData = new Date(this.endDate).getTime();
//        if (endData - myDate < 0) {
//            for (let i = 0; i < this.components.length; i++) {
//                for (let j = 0;j < this.components[i].children.length;j++) {
//                }
//            }
//        }
        // 广告
        this.add();
    },
    add() {
        // 在页面中定义插屏广告
        let interstitialAd = null;

        // 在适合的场景显示插屏广告
        if (interstitialAd) {
            interstitialAd.show().catch((err) => {
                console.error(err);
            });
        }
    },
    showAnimate(e){
        console.log("weapp:"+e)
        router.push({
            uri:e
        });
    },
};
