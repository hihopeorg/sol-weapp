import pasteboard from '@ohos.pasteboard';
import prompt from '@system.prompt';
export default{

    /**
   * 页面的初始数据
   */
    data: {
        bigImag:false,
    },
    // 图片点击放大
    previewImg() {
        //图片预览
        setTimeout(()=>{
            this.bigImag = true;
        },100)
    },
    smallImg(){
        this.bigImag = false;
    },
    copy() {
        pasteboard.createUriRecord("https://github.com/sunniejs/sol-weapp/");
        setTimeout(()=>{
            prompt.showToast({
                message: "复制成功",
                duration: 2000,
            });
        },500);
    },
}