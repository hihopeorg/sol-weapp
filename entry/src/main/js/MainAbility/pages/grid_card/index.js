import prompt from '@system.prompt';
export default {
    data: {
        // 九宫格数据
        card: [
            {
                id: 1,
                prizeName: '10金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0, //   :0 反面 , 1 正面
                isMove:false
            },
            {
                id: 2,
                prizeName: '10金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 3,
                prizeName: '100金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 4,
                prizeName: '10金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 5,
                prizeName: '40金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 6,
                prizeName: '20金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 7,
                prizeName: '50金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 8,
                prizeName: '60金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            },
            {
                id: 9,
                prizeName: '10金币',
                img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                status: 0,
                isMove:false
            }
        ],
        readyed: false // 是否点击开始抽奖
    },
    started() {
        if (this.readyed) {
            prompt.showToast({
                message: "已经开启抽奖",
                duration: 2000,
            });
            return
        }
        // 触发组件开始方法
        this.$child("sol-grid-card").start();
        this.readyed = true;
    },

    // 子组件触发，点击打开单个卡片奖品
    openCard(e) {
        const { item, index } = e._detail
        // 动画没有结束，或已经点开
        if (!this.readyed || item.status == 1) {
            return
        }
        // 改变卡片翻转状态 status :0 反面 , 1 正面
        for(let i=0;i<this.card.length;i++){
            if(i==index){
                this.card[i].status = 1;
            }
        }
        prompt.showToast({
            message: "你点击了第"+(index + 1)+"个",
            duration: 2000,
        });
    },
}
