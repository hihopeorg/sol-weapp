
const getList = (count = 30, step = 0) => [...new Array(count)].map((n, i) => ({
    title: `我是小仙女,爱你第 ${i + step}遍`,
    content: 'Wux Weapp'
}))
export default {
    data: {
        scrollTop: 0,
        scrollIng: false,
        items: [],
        count: 0,
    },
    onInit() {
        this.loadMore()
    },
    loadMore() {
        this.items = [...this.items, ...getList(30, this.count)];
        this.count = this.count + 30;
    },
    // 达到底部
    onReachBottom(){
        this.loadMore()
    },
    // 页面滚动
    onPageScroll() {
        this.$child("sol-icon").hideIcon();
    },
    showIcon(){
        this.$child("sol-icon").showIcon();
    },
}
