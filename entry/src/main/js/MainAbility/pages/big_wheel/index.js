import prompt from '@system.prompt';
export default {
    data: {
        award: 1,
        mode: 2, // 旋转模式
        spin:true,
        awardList: [
            { title: '10个金币' },
            { title: '20个金币' },
            { title: '30个金币' },
            { title: '50个金币' },
            { title: '80个金币' },
            { title: '200个金币' }
        ] // 顺时针对应每个奖项
    },
    // 用户点击开始抽奖
    wheelStart() {
        // 设置奖项
        this.award=Math.floor(Math.random() * 6 + 1); //安全起见生成奖项应该由后端完成，生成1到6随机
        // 触发组件开始方法
        this.$child("sol-wheel").begin(this.award);
    },
    // 抽奖完成后操作
    wheelSuccess() {
        const index = this.award - 1;
        prompt.showToast({
            message: "恭喜你获得"+this.awardList[index].title,
            duration: 2000,
        });
    },
    // 切换模式
    switchMode(e) {
        this.mode=e;
    },
}
