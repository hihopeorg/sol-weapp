export default {
    data: {
      options:  {
        duration: 2500,        // :number 动画时间
        height: 50,       // :number 滚动行高 px
        width: '100%',    // 组件整体宽度
        ease: 'cubic-bezier(0, 1, 0, 1)',   // 动画过渡效果
        color: '#02BE8A', // 字体颜色
        columnStyle: 'font-size:48px;',  // 字体单元 覆盖样式
      }
    },
    onReady() {
        this.start("animate0");
        this.start("animate1");
    },
    // 开始
    start(id) {
        this.$child(id).start();
    },
    // 重置
    reset(id) {
        this.$child(id).start();
    },
}
