# sol-weapp

## 简介

> 提供带有大转盘功能，红包雨功能，九宫格翻牌功能，slot抽奖功能，滚动导航条功能，悬浮按钮，滚动数字功能的集成组件。。

![avatar](./gifs/big-wheel.gif)![avatar](./gifs/grid-card.gif)
![avatar](./gifs/packet-rain.gif)![avatar](./gifs/slot-machine.gif)
![avatar](./gifs/scoll-nav.gif)![avatar](./gifs/float-icon.gif)
![avatar](./gifs/animate-number.gif)

## 下载安装

```shell
npm install @ohos/sol_weapp --save
```

OpenHarmony
npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

###### 1 大转盘效果

###### hml中实现：

   ```
<element name='sol-wheel' src='../../dist/wheel/index.hml'></element>
<div class="page-wheel">
    <sol-wheel id="sol-wheel" award-numer="{{award}}" mode="{{mode}}" @start="wheelStart" @success="wheelSuccess"></sol-wheel>
    <div class="wheel-mode">
        <text class="row">旋转模式：</text>
        <text class="sol-button  {{mode=='1'?'sol-button--danger':''}}" onclick="switchMode(1)">
            指针旋转
        </text>
        <text class="sol-button  {{mode=='2'?'sol-button--danger':''}}" onclick="switchMode(2)">
            转盘旋转
        </text>
    </div>
</div>
   ```

###### js中实现：

   ```
   export default {
       data: {
           award: 1,
           mode: 2, // 旋转模式
           spin:true,
           awardList: [
               { title: '10个金币' },
               { title: '20个金币' },
               { title: '30个金币' },
               { title: '50个金币' },
               { title: '80个金币' },
               { title: '200个金币' }
       },
   }
   ```

###### 2 红包雨效果

###### hml中实现：

   ```
<element name='sol-packet-rain' src='../../dist/packet-rain/index.hml'></element>
<div>
    <div class="container">
        <image mode="aspectFit" src="https://www.sunniejs.cn/static/weapp/logo.png"></image>
        <text class='title'>Sol Weapp</text>
        <text>红包雨</text>
    </div>
<!-- 红包雨组件 -->
    <sol-packet-rain visible="{{visible}}" create-speed="{{createSpeed}}" time="{{time}}" ready-time="{{readyTime}}" min="{{min}}" max="{{max}}" @finish="success"></sol-packet-rain>
</div>
   ```

###### js中实现：

   ```
     export default{
         data: {
             visible: false
         },
     }
   ```

###### 3 九宫格翻牌

###### hml中实现：

   ```
<element name='sol-grid-card' src='../../dist/grid-card/index.hml'></element>
<div class="container">
    <div class="grid-card">
        <sol-grid-card id="sol-grid-card" card="{{card}}"  @open="openCard"></sol-grid-card>
    </div>
    <div>
        <div class="sol-button" @click="started">
            <text>开始抽奖</text>
        </div>
    </div>
</div>
   ```

###### js中实现：

   ```
    export default{
         data:{
            card: [
               {
                   id: 1,
                   prizeName: '10金币',
                   img: 'https://www.sunniejs.cn/static/weapp/prize.png',
                   status: 0, //   :0 反面 , 1 正面
                   isMove:false
               },
               xxxx 九个对象
                 ],
            readyed: false            
        }
    }
   ```

###### 4 slot效果

###### hml中实现：

   ```
<element name='sol-slot-machine' src='../../dist/slot-machine/index.hml'></element>
<div class="container">
    <sol-slot-machine id="sol-slot-machine" result="{{result}}" @finish="doFinish"></sol-slot-machine>
    <div class="btn-group">
        <text class="sol-button" onclick="start">开始</text>
    </div>
</div>
   ```

###### js中实现：

   ```
     export default{
         data:{
            result: []            
         }
     }
   ```

###### 5 滚动导航条

###### hml中实现：

   ```
<element name='sol-scoll-nav' src='../../dist/scoll-nav/index.hml'></element>
<div>
  <sol-scoll-nav tabs="{{list}}" current="{{index}}" @change="onTabsChange"></sol-scoll-nav>
  <swiper class="container" index="{{index}}" indicator="false" duration="300" loop="false" @change="onSwitchTab">
    <div for="{{item in list}}">
      <text class= "content">
        {{$idx}}--{{item.title}}
      </text>
    </div>
  </swiper>
</div>
   ```

###### js中实现：

   ```
    export default{
         data:{
            list: [
               { num: 20, title: '全部', id: '097b0651ca000031b193e2bca6be792d' },
               xxxxx 多个导航
               ],
            index: 0           
         }
    }
   ```

###### 6 悬浮按钮

###### hml中实现：

   ```
<element name='sol-icon-container' src='../../dist/icon-container/index.hml'></element>
<list class="app-container"  @touchstart="onPageScroll"  @touchend="showIcon" @scrollbottom="onReachBottom">
    <list-item for="{{ item in items}}">
        <text class="cell" >
            {{item.title}}
        </text>
    </list-item>
<!--icon -->
    <sol-icon-container id="sol-icon" height="45" top="0" bottom="100">
        <div  slot="icons" >
            <div class="float-icon">
                <image src="https://www.sunniejs.cn/static/weapp/home-icon.png" mode="widthFix" />
            </div>
        </div>
    </sol-icon-container>
</list>
   ```

###### js中实现：

   ```
    export default{
         data:{
            scrollTop: 0,
            scrollIng: false,
            items: [],
            count: 0,         
         }
    }
     
   ```

###### 7 滚动数字

###### hml中实现：

   ```
<element name='sol-animate-number' src='../../dist/animate-number/index.hml'></element>
<div class="app-container">
    <div class="item">
        <text class="title">默认:</text>
        <sol-animate-number random="true" id="animate0"  value="{{100}}" min="{{50}}" max="{{100}}"></sol-animate-number>
    </div>
    <div class="sol-div-button">
        <text class="sol-button sol-button--blue" data-id="animate0" onclick="reset('animate0')">重新加载</text>
    </div>
    <div class="item">
        <text class="title">随机数组:</text>
        <sol-animate-number random="true" id="animate1"  value="{{100}}" min="{{50}}" max="{{150}}" len="{{100}}"></sol-animate-number>
    </div>
    <div class="sol-div-button">
        <text class="sol-button sol-button--blue"  data-id="animate1" onclick="reset('animate1')">重新加载</text>
    </div>
</div>
   ```

## 接口说明

1. 大转盘开始转动
   `begin(e)`
2. 开始九宫格动画
   `start()`
3. slot开始
   `start()`
4. 隐藏悬浮按钮
   `hideIcon()`
5. 显示悬浮按钮
   `showIcon()`
6. 开始滚动数字动画
   `start()`
7. 红包雨点击事件
   `handleClickRain(e)`

## 兼容性

支持 OpenHarmony API version 8 及以上版本。

## 目录结构

````
|---- sol-weapp
|     |---- entry  # 示例代码文件夹
|     |---- sol-weapp  # sol-weapp库文件夹
|         |---- src
|             |---- main
|                  |---- js
|                      |---- animate-number
|                          |---- index.hml  # 滚动数字
|                          |---- index.js  # 滚动数字
|                          |---- index.css  # 滚动数字
|                      |---- canvas-drawer
|                          |---- index.hml  # 画布绘制
|                          |---- index.js  # 画布绘制
|                          |---- index.css  # 画布绘制         
|                      |---- grid-card
|                          |---- index.hml  # 九宫格
|                          |---- index.js  # 九宫格
|                          |---- index.css  # 九宫格
|                      |---- icon-container
|                          |---- index.hml  # 图片容器
|                          |---- index.js  # 图片容器
|                          |---- index.css  # 图片容器 
|                      |---- packet-rain
|                          |---- index.hml  # 红包雨
|                          |---- index.js  # 红包雨
|                          |---- index.css  # 红包雨 
|                      |---- wheel
|                          |---- index.hml  # 大转盘
|                          |---- index.js  # 大转盘
|                          |---- index.css  # 大转盘    
|                      |---- slot-machine
|                          |---- index.hml  # slot
|                          |---- index.js  # slot
|                          |---- index.css  # slot           
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/sol-weapp/issues)
给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/sol-weapp/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/hihopeorg/sol-weapp/blob/master/LICENSE) ，请自由地享受和参与开源。