export default{
    externalClasses: ['sol-class'],
    props: {
        tabs: {default:[]},
        current: {default:0},
    },
    data: {
        showNavDrap: false,
        activeIndex: 0,
        navScrollLeft: 0
    },
    onInit(){
        this.activeIndex = this.current;
        this.$watch("current","changeIndex");
    },
    changeIndex(){
        this.activeIndex = this.current;
    },
    showAllNav() {
        this.showNavDrap=!this.data.showNavDrap;
    },
    /**
     * 触发 点击事件
     */
    onClick(index) {
        this.activeIndex=index;
        this.$emit("change",index)
        this.showNavDrap = false;
    },

    moveMiddle(title,e){
        let amVtabsSliders = this.$element('nav3');
        amVtabsSliders.scrollBy({
            dx: e.touches[0].globalX - e.touches[0].localX -150 + (title.length*10)
        });
    },
    bidClick(index){
        this.activeIndex=index;
        this.$emit("change",index)
        this.showNavDrap = false;
        this.updated(index);
    },
    updated(index) {
        let fontLength=0
        for(let i=0;i<this.tabs.length;i++){
            if(index >= i){
              fontLength = fontLength + (this.tabs[i].title.length*15+30);
            }
        }
        let moves = this.$element('nav3');
        moves.scrollBy({
            dx: fontLength -170
        });
    }

}
