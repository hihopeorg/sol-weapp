const CONFIG = {
    duration: 2000, // :number 动画时间
    height: 50, // :number 滚动行高 px
    width: "100%", // 组件整体宽度
    easing: "cubic-bezier(0, 1, 0, 1)", // 动画过渡效果
    color: "#000", // 字体颜色
    columnStyle: "", // 字体单元 覆盖样式
    delay: 500,
    fill: 'forwards',
    iterations: 1,
    direction: 'normal',
};
const LOCK = 500; // 锁止
import animator from '@ohos.animator';
export default{
    props: {
        value: {default:String},
        // min 最小值 不传默认为 0
        min: {default:String},
        // 是否生成随机
        random: {default:false},
        // max 最大值 不传默认为 value, random 为true 生效
        max: {default:String},
        // len 随机数字个数 ，random 为true 生效
        len: {default:40},
        options: {default:{}},
    },
    data: {
        columns: [],
        animationData: null,
        animation: null,
        translateVal: 0,
        randomstop:false,
        restarted:true,
    },
    attached() {
        this.renderStyle();
    },
    start() {
        this.restarted = true;
        if(!this.options.duration){
            this.options = JSON.parse(JSON.stringify(CONFIG));
        }
          let { animationData } = this;
          if (animationData) {
            this.reset().then(()=>{
              this.run()
            })
          }else{
            this.run()
          }
      },
    run(){
      let { options } = this;
      // 创建数组
      const arr = this.setRange();
      this.columns=arr;
      this.animation = animator.createAnimator(options);
        setTimeout(()=>{
            this.scrollNUmber(options);
        },10)
    },
    scrollNUmber(options){
        var _this = this;
        const arr = this.setRange();
        let translateHeight = 0;
        let heighted =0;
        let timers = setInterval(()=> {
            if((0-translateHeight) > (arr.length - 5) * options.height){
                heighted = 3;
            }else{
                heighted = 30;
            }
            translateHeight = translateHeight - heighted;
            _this.translateVal = translateHeight + "px";
            if (translateHeight < (-(arr.length - 1) * options.height)) {
                translateHeight = -(arr.length - 1) * options.height + "px";
                clearInterval(timers);
            }
        },30)
        this.animation.play();
        this.animation.onfinish = function() {
            setTimeout(()=>{
                this.restarted = false;
            },1)
        };
    },
    // 重置
    reset() {
    },

    // 生成打乱的数组
    randArray(len, min, max) {
        return Array.from(
            { length: len },
            () => Math.floor(Math.random() * (max - min)) + min
        );
    },
    // 设置滚动数字
    setRange() {
        let { max, min, value, random, len } = this;
        let arr = [];
        // 最小值
        if (isNaN(min)) {
            min = 0;
        }
        if (isNaN(value)) {
            value = 0;
        }
        if (isNaN(max)) {
            max = value;
        }
        if (random) {
            min = parseInt(min);
            max = parseInt(max);
            if (min >= max) {
                min = max;
                return [value];
            }
            if (parseInt(len) > LOCK) {
                len = LOCK;
            }
            arr = this.randArray(len, min, parseInt(max) - 1);
            arr.push(value);
            return arr;
        } else {
            min = parseInt(min);
            max = parseInt(value);
            if (min > max) {
                min = max;
            }
            if (parseInt(max) - parseInt(min) > LOCK) {
                min = parseInt(max) - LOCK;
            }
            for (let i =  parseInt(min); i <=  parseInt(value); i++) {
                arr.push(i);
            }
            return arr;
        }
    },
    renderStyle() {
        let options = this.options;
        Object.keys(options).map((i) => {
            let val = options[i];
            switch (i) {
                case "duration":
                case "height":
                    if (parseInt(val) || val === 0 || val === "0") {
                        options[i] = val;
                    }
                    break;
                default:
                    val && (options[i] = val);
                    break;
            }
        });
        this.options=options;
    },
};
