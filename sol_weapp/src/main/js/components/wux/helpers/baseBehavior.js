/**
 * Simple bind, faster than native
 *
 * @param {Function} fn
 * @param {Object} ctx
 * @return {Function}
 */
const bind = (fn, ctx) => {
    return (...args) => {
        return args.length ? fn.apply(ctx, args) : fn.call(ctx)
    }
}

/**
 * Object assign
 */
const assign = (...args) => Object.assign({}, ...args)

export default{
    props: {
        visible: {default:false},
    },
    onInit () {
        this.fns = {}
    },
  /**
   * 合并参数并绑定方法
   *
   * @param {Object} opts 参数对象
   * @param {Object} fns 方法挂载的属性
   */
  $$mergeOptionsAndBindMethods (opts = {}, fns = this.fns) {
      const options = Object.assign({}, opts)
      for (const key in options) {
          fns[key] = bind(options[key], this)
          delete options[key]
      }
      return options
  },
  /**
   * Promise setData
   * @param {Array} args 参数对象
   */
  $$setData (...args) {
      const params = assign({}, ...args)

      return new Promise((resolve) => {
          this.setData(params, resolve)
      })
  },
  /**
   * 延迟指定时间执行回调函数
   * @param {Function} callback 回调函数
   * @param {Number} timeout 延迟时间
   */
  $$requestAnimationFrame (callback = () => {}, timeout = 1000 / 60) {
      return new Promise((resolve) => setTimeout(resolve, timeout)).then(callback)
  },
}
