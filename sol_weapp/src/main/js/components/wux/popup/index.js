import { $wuxBackdrop } from '../index'

export default{
  props: {
    title: {default:''},
    content: {default:''},
    extra: {default:''},
    position: {default:''},
    wrapStyle: {default:''},
    closable: {default:false},
    mask: {default:true},
    maskClosable: {default:true},
    visible: {default:false},
    classNames: {default:'fadeIn'},
    transparent:{default:false},
  },
  data: {
    transitionName: '',
    hidden: true
  },
  onInit() {
    this.getTransitionName()
    if (this.data.mask) {
      this.$wuxBackdrop = $wuxBackdrop('#wux-backdrop', this)
    }
  },
    move() {
      // 禁止背景滚动
      return false
    },
    close() {
      this.$emit('close')
    },
    onMaskClick() {
      if (this.maskClosable) {
        this.close()
      }
    },
    onExited() {
      this.setData({ hidden: true })
      this.$emit('closed')
    },
    getTransitionName(value = this.data.classNames) {
      this.transitionName = `wux-animate--${value}`
    },
    onVisibleChange(visible) {
      if (visible && this.hidden) {
        this.hidden = false
      }
      if (this.mask && this.$wuxBackdrop) {
        this.$wuxBackdrop[visible ? 'retain' : 'release']()
      }
      this.$emit('change', { visible })
    },
}
