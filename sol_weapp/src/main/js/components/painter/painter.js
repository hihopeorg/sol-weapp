import Downloader from './lib/downloader';

const util = require('./lib/util');

const downloader = new Downloader();

export default{
  props: {
    customStyle: {default:''},
    palette: {default:''},
    widthPixels: {default:0},
    // 启用脏检查，默认 false
    dirty: {default:false}
  },

  data: {
    picURL: '',
    showCanvas: true,
    painterStyle: '',
  },
    /**
     * 判断一个 object 是否为 空
     * @param {object} object
     */
    isEmpty(object) {
      for (const i in object) {
        return false;
      }
      return true;
    },

    isNeedRefresh(newVal, oldVal) {
      if (!newVal || this.isEmpty(newVal) || (this.dirty && util.equal(newVal, oldVal))) {
        return false;
      }
      return true;
    },

    startPaint() {
      if (this.isEmpty(this.palette)) {
        return;
      }
      let screenK = 750;
      setStringPrototype(screenK, 1);
      
      this.downloadImages().then((palette) => {
        const {
          width,
          height
        } = palette;
        
        if (!width || !height) {
          console.error(`You should set width and height correctly for painter, width: ${width}, height: ${height}`);
          return;
        }
        this.canvasWidthInPx = width.toPx();
        if (this.widthPixels) {
          // 如果重新设置过像素宽度，则重新设置比例
          setStringPrototype(screenK, this.canvasWidthInPx)
          this.canvasWidthInPx = this.widthPixels
        }
  
        this.canvasHeightInPx = height.toPx();
        this.setData({
          painterStyle: `width:${this.canvasWidthInPx}px;height:${this.canvasHeightInPx}px;`,
        });
        this.saveImgToLocal();
      });
    },

    downloadImages() {
      return new Promise((resolve) => {
        let preCount = 0;
        let completeCount = 0;
        const paletteCopy = JSON.parse(JSON.stringify(this.palette));
        if (paletteCopy.background) {
          preCount++;
          downloader.download(paletteCopy.background).then((path) => {
            paletteCopy.background = path;
            completeCount++;
            if (preCount === completeCount) {
              resolve(paletteCopy);
            }
          }, () => {
            completeCount++;
            if (preCount === completeCount) {
              resolve(paletteCopy);
            }
          });
        }
        if (paletteCopy.views) {
          for (const view of paletteCopy.views) {
            if (view && view.type === 'image' && view.url) {
              preCount++;
              /* eslint-disable no-loop-func */
              downloader.download(view.url).then((path) => {
                view.url = path;
              }, () => {
                completeCount++;
                if (preCount === completeCount) {
                  resolve(paletteCopy);
                }
              });
            }
          }
        }
        if (preCount === 0) {
          resolve(paletteCopy);
        }
      });
    },
};


function setStringPrototype(screenK, scale) {
  /* eslint-disable no-extend-native */
  /**
   * 是否支持负数
   * @param {Boolean} minus 是否支持负数
   */
  String.prototype.toPx = function toPx(minus) {
    let reg;
    if (minus) {
      reg = /^-?[0-9]+([.]{1}[0-9]+){0,1}(rpx|px)$/g;
    } else {
      reg = /^[0-9]+([.]{1}[0-9]+){0,1}(rpx|px)$/g;
    }
    const results = reg.exec(this);
    if (!this || !results) {
      console.error(`The size: ${this} is illegal`);
      return 0;
    }
    const unit = results[2];
    const value = parseFloat(this);

    let res = 0;
    if (unit === 'px') {
      res = Math.round(value * screenK * (scale || 1));
    } else if (unit !== 'px') {
      res = Math.round(value * (scale || 1));
    }
    return res;
  };
}