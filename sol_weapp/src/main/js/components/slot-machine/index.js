export default{
    externalClasses: ['sol-class'],
    props: {
        // 中奖结果
        result:{default:[]},
    },
    data: {
        credits: 50, //积分
        curBet: 1, // 每局消耗积分
        stripHeight: 720, // 总高度
        alignmentOffset: 100, // 结果位置偏移量
        reelSpeed1Delta: 200, // 间隔位移
        positioningTime: 200, // 停止前动画时间
        bounceHeight: 200, // 结束弹射动画高度
        firstReelStopTime: 1667, // 第 一个动画延迟停止时间
        secondReelStopTime: 1575, // 第二个动画延迟停止时间
        thirdReelStopTime: 1568, // 第三个动画延迟停止时间
        payoutStopTime: 1500, // 触发结束延迟时间
        numIconsPerReel: 6, // 每个轮子几个格子
        timer: [],
        reels: [
            // 轮子动画属性
            {
                top: -25, // 初始位置
                animation: '', // 结束滚动动画
                css: '' // 反弹动画css
            },
            {
                top: -90,
                animation: '',
                css: ''
            },
            {
                top: -225,
                animation: '',
                css: ''
            }
        ]
    },
    start() {
        const { firstReelStopTime, secondReelStopTime, thirdReelStopTime, result, payoutStopTime } = this
        // 开始滚动
        this.start_reel_spin(0)
        this.start_reel_spin(1)
        this.start_reel_spin(2)
        // 结束
        runAsync(firstReelStopTime)
            .then(() => {
                this.stop_reel_spin(0, result[0])
                return runAsync(secondReelStopTime)
            })
            .then(() => {
                this.stop_reel_spin(1, result[1])
                return runAsync(thirdReelStopTime)
            })
            .then(() => {
                this.stop_reel_spin(2, result[2])
                return runAsync(payoutStopTime)
            })
            .then(() => {
                // 完成
                // 重置
                this.reset_reel_spin(0);
                this.reset_reel_spin(1);
                this.reset_reel_spin(2);
                this.$emit("finish");
            })
    },
    // 开始动画
    start_reel_spin(index) {
        const { stripHeight, reelSpeed1Delta } = this
        const position = parseInt(-(Math.random() * stripHeight * 2));
        this.reels[index].top = position;
        // 循环动画
        this.timer[index] = setInterval(() => {
            this.reels[index].top = this.reels[index].top + reelSpeed1Delta
            if (this.reels[index].top > 0) {
                this.reels[index].top =  2 * -stripHeight;
            }
        }, 2)
    },
    // 停止动画
    stop_reel_spin(index, lottery) {
        const { stripHeight, numIconsPerReel, alignmentOffset} = this
        const cellHeight = stripHeight / numIconsPerReel
        const position = -stripHeight - (lottery - 1) * cellHeight + alignmentOffset
        // 清除滚动timer
        clearInterval(this.timer[index])
        // 最终位置
        this.reels[index].top =  position - stripHeight;
        // 到最终位置之前的动画
    },
    // 重置动画
    reset_reel_spin(index) {
        this.reels[index].css="";
    }
}
function runAsync(time) {
    return new Promise(function(resolve) {
        const timer = setTimeout(function() {
            resolve()
            clearTimeout(timer)
        }, time)
    })
}
