/*
 * @Author: Sunnie 
 * @Date: 2021-09-06 18:42:25 
 * @Last Modified by: Sunnie
 * @Last Modified time: 2021-09-12 18:55:13
 */
// 该接口是为获取动态二维码，并未对接开发者可自行修改
// import { getCodeUrl } from "../../api/common"
export default{
    props: {
        shareData: {default:{}},
        // 展示方式
        mode: {
            type: String,
            value: "bar" //  bar: 分享弹出，可以转发好友，或者生成图片  popup: 无分享弹窗，直接显示图片弹出 path: 直接返回图片路径
        },
        // 显示底部弹窗
        visible: {default:false},

        // 弹出背景透明
        transparent: {default:false},
    },
    data: {
        // ========== 需要生成动态二维码，打开此段以下代码 start ==========
        // cache: {
        //  },
        // ========== 需要生成动态二维码，打开此段以上代码 end ==========
        imgVisible: false, // 生成弹窗
        painting: false, // 锁
        shareMsgData: {}, // 分享给好友数据，不能使用shareData污染之后会重新渲染
        sharePosterData: {}, // 海报图数据，不能使用shareData污染之后会重新渲染
        paintingCard: false, // 当前是否在绘制分享给好友分享图
        imgPath: ""
    },
    onInit(){
        this.paintStart();

    },
    // 开始绘画
    paintStart() {
        console.log('paintStart')
        const {
            shareData,
            mode
        } = this
        // 默认
        const defaults = {
            cardPoster: false,
            shareType: '',
            shareParams: '',
            shareTitle: '',
            sharePath: '',
            shareImg: ''
        }
        const shareMsgData = Object.assign({}, defaults, shareData)
        // 设置默认数据
        this.shareMsgData = shareMsgData;
        this.shareImage = '';
        // 分享给好友的卡片图需要生成图片
        if (shareData.cardPoster) {
            this.drawCardCanvas()
        }
        // 返回路径直接生成图片，直接生成弹窗图片
        if (mode == 'path') {
            this.drawPosterCanvas()
        }

    },
    // 分享给好友卡片图合成
    drawCardCanvas() {
        this.paintingCard = true // 挡墙
    },
    // 海报图合成
    drawPosterCanvas() {
        const {
            shareImage
        } = this

        // 如果已经生成过了图
        if (shareImage && shareImage.indexOf("//tmp") !== -1) {
            // 如果画过了就赋值,就会接显示
            this.visible = false;
            this.imgVisible = true;
            return false
        }
        // 锁
        if (this.painting) return false

        this.painting = true
    },
    // 关闭显示弹窗
    onImgClose() {
        this.swiperIndex = 0;
        this.imgVisible = false;
    },
    // 关闭分享弹窗
    onShareClose() {
        this.visible = false;
    },

    // 图片生成成功
    onImgOK(e) {
        // 解锁
        this.painting = false
        const imgPath = e;
        this.imgPath = imgPath
        // 如果当前正在绘制分享给好友的分享图
        if (this.paintingCard) {
            // 单独赋值不需要合成的时候
            this.shareMsgData.shareImg = imgPath
            // 完成后设置false
            this.paintingCard = false
            return false
        }
        // 直接返回路径模式
        if (this.mode == 'path') {
            this.triggerEvent('path', imgPath)
            return false
        }
        // 直接弹窗模式
        if (this.data.mode == 'popup') {
            this.shareImage = imgPath;
            this.imgVisible = true;
            return false;
        }
        this.visible = false;
        this.shareImage = imgPath;
        this.imgVisible = true;
    },
    // 获取二维码，缓存里有就从缓存里拿
    getCacheImage(parmas) {
        // 生成唯一缓存键值
        const url = parmas.page + parmas.scene
        return new Promise((resolve) => {
            if (this.cache[url]) {
                resolve(this.cache[url])
            }
        })
    }
}
