export default{
    props: {
        // 九宫格卡片
        card: {default:{}}
    },
    data: {
        move: ''
    },
    start(callback) {
        const { card } = this
        runAsync(100)
            .then(() => {
                // 延迟100毫秒翻转第一排牌面
                for (let i = 0; i < 3; i++) {
                    card[i].status = 1;
                }
                this.card = card;
                return runAsync(200)
            })
            .then(() => {
                // 延迟200毫秒翻转第二排牌面
                for (let i = 3; i < 6; i++) {
                    card[i].status = 1;
                }
                this.card = card;
                return runAsync(200)
            })
            .then(() => {
                // 延迟200毫秒翻转第三排牌面
                for (let i = 6; i <= 8; i++) {
                    card[i].status = 1;
                }
                this.card = card;
                return runAsync(800)
            })
            .then(() => {
                // 将所有背面朝上
                for (let i = 0; i < 9; i++) {
                    card[i].status = 0;
                }
                this.card = card;
                return runAsync(1000)
            })
            .then(() => {
                // 洗牌动画
                for (let i = 0; i < 9; i++) {
                    runAsync(i * 40)
                        .then(() => {
                            card[i].isMove = true
                            this.card = card;
                            return true
                        })
                        .then(() => {
                            card[i].isMove = false;
                            this.card = card;
                            return true
                        })
                        .then(() => {
                            // 结束后回调
                            if (typeof callback === 'function') {
                                callback();
                            }
                        })
                }

            })
    },
    // 点击打开单个卡片，开奖
    openCard(item,index) {
//        // 触发父组件方法
        this.$emit("open",{item,index})
    }
}
function runAsync(time) {
    return new Promise(function(resolve) {
        const timer = setTimeout(function() {
            resolve()
            clearTimeout(timer)
        }, time)
    })
}
