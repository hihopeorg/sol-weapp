//const innerAudioContext = wx.createInnerAudioContext()
 let readyTimer = null
 let rainTimer = null
 let redEnvelopes = []
 let animation = null
 const minWidth = 30  // 红包图片最小宽度
 const maxWidth = 40 // 红包图片最大宽度

export default{
  props: {
     // 是否开始展示游戏
    visible: {default:false},
    // 游戏时间
    time: {default:10},
     // 倒计时单位秒
    readyTime: {default:3},
    //  速度
    createSpeed: {default:5},
     // 单个最小金额
    min: {default:0},
    // 单个最大金额
    max: {default:3},
    redImg: {default:""},
    openImg: {default:""},
  },
  data: {
    showRainTotalTime:10,  // 红包雨时间
    showStatus: 1, // 红包雨状态：1:准备倒计时，2:正在红包雨，3:红包雨结束
    windowWidth: 720,
    windowHeight: 1080,
    rainResult: {},
    loading: false,
    showScore:0,
    showChangeScore:0,
    scoreStyle:'',
    progressAni:120,
    context:'',
    top:-100,
    left:-100,
  },
  onReady(){
    //防止页面刚进入红包雨时候  先显示默认时间的bug  即data中的时间 ===> showRainTotalTime:10,  // 红包雨时间
    this.showRainTotalTime = parseInt(this.time)
     // 重置
    redEnvelopes = [] 
    clearTimeout(readyTimer) 
    clearTimeout(rainTimer)  
    this.cancelCustomAnimationFrame(animation)
     // 开始准备倒计时
     this.cultdown()
//      const { windowWidth , windowHeight } = APP.globalData.systemInfo
//      this.windowWidth = windowWidth
//      this.windowWidth = windowHeight
  },
  detached() {
    readyTimer && clearInterval(readyTimer)
    rainTimer && clearInterval(rainTimer)
    animation && this.cancelCustomAnimationFrame(animation)
  },
    // 开始准备倒计时
    cultdown() {
      let _this=this
      let readyTime= parseInt(this.readyTime)
      readyTimer=setInterval(function () {
        if(--readyTime <= 0 ){
          clearInterval(readyTimer)
         // 显示红包雨
          _this.showRain()
        } 
        _this.readyTime=readyTime;
      }, 1000)
    },
    // 展示红包雨界面
    showRain() {
      let _this=this
      // 显示红包雨
      this.showStatus=2;
      // 初始化红包雨
      setTimeout(()=>{
        this.initRain();
      },1)

      // 倒计时进度条
      this.ininProgress()
      // 红包雨倒计时
      let showRainTotalTime = parseInt(this.time)
      rainTimer= setInterval(function () {
          if(--showRainTotalTime <= 0 ){
            clearInterval(rainTimer);
            if(animation){
              // 结束
              _this.showRainResult();
              _this.cancelCustomAnimationFrame(animation);
            }
          }
          _this.showRainTotalTime = showRainTotalTime;
        }, 1000);
    },
    // 倒计时进度条
    ininProgress(){
      let setCut = 130/parseInt(this.time);
      let timer = setInterval(()=>{
        this.progressAni = this.progressAni - setCut/10;
        if(this.progressAni<=0){
          clearTimeout(timer)
        }
      },100)
    },
    // 关闭
    handleClose() {
      this.$emit("finish");
    },
    // 显示结果
    showRainResult() {
      // 结束动画

      this.showStatus = 3;
      this.rainResult = {amount: 100};
    },
    // 红包下落函数
    customRequestAnimationFrame(e) {
      let _this = this
      let timer = setTimeout(function () {
          e.call(_this), clearTimeout(timer);
        }, 1000 / 60)
      return timer
    },
    // 清除红包下落函数
    cancelCustomAnimationFrame(e) {
      e && (clearTimeout(e), animation = null)
    },
    // 开始下落
    doDrawRain () {
      const context = this.context
      const {windowWidth,windowHeight } = this;
      context.clearRect(0, 0,windowWidth, windowHeight);
      for (let n = 0; n < redEnvelopes.length; n += 1) {
        if(redEnvelopes[n].y>=400){
          context.clearRect(0, 570,windowWidth, windowHeight);
        }
        const i = redEnvelopes[n]; // 红包
        const {x,y,vx,vy,width,height,open}=i;
        let img = new Image();
        console.info("src----"+this.redImg)
        img.src = open ? this.openImg : this.redImg;
        const imgWidth  = open ? width + 20 :width;
        const imgHeight = open ? height + 25 :height;
        context.drawImage(img, x, y,imgWidth,imgHeight);
            i.x += vx;
            i.y += vy;
            i.y >= windowHeight && (i.y = 0, i.open =false);
            i.x + width <= 0 && (i.x = windowWidth - width, i.open = false);
      }

      //TODO  红包雨未展示
//       context.draw()
      // 下落函数
       animation= this.customRequestAnimationFrame(this.doDrawRain);
    },
    // 随机数
    randNum: function (min, max) {
      return Math.floor(min + Math.random() * (max - min));
    },
    // 准备红包雨下落
    initRainDrops() {
     const {windowWidth,windowHeight,max,min} = this;
      for (let n = 0; n < 10; n += 1) {
        const startX = Math.floor(Math.random() *windowWidth);
        const startY = Math.floor(Math.random() *windowHeight);
        // 红包图片宽度大小30~40
        const width = this.randNum(minWidth, maxWidth);
        // 宽度为红包高度的百分之八十
        const height = Math.floor(width / .8);
        // 速度
        const vy = 1 * Math.random() + parseInt(this.createSpeed);
        // 红包金额
        const score = this.randNum(min,max+1);
          redEnvelopes.push({
            x: startX,
            y: startY,
            vx: -1,  // x轴速度
            vy: vy, // y轴速度
            score:score,
            width: width,
            height: height,
            open: false
          });
      }
        this.doDrawRain();
    },
    // 点击红包事件
    handleClickRain(e) {
      let touch = e.touches[0];
      let touchX =touch.globalX;
      let touchY =touch.globalY;
      let _this=this
      for (let o = 0; o < redEnvelopes.length; o += 1) {
        let i = redEnvelopes[o],
        rainX = i.x,
        rainY = i.y,
        width = i.width,
        height = i.height,
        gapX = touchX - rainX,
        gapY = touchY - rainY;
        if (gapX >= -20 && gapX <= width + 20 && gapY >= -20 && gapY <= height + 20) {
          this.top = touch.localX;
          this.left = touch.localY;
          i.open = true;
          let score = _this.showScore + i.score;
          _this.showScore = score;
          _this.showChangeScore = i.score ;
          break;
        }
      }
    },
    // 初始化 canvas
    initRain() {
        const el = this.$element('drawImage');
        this.context = el.getContext('2d');
//        this.redEnvelopeImg = "/components/packet-rain/images/red-packet-rain.png";
//        this.openEnvelopeImg = "/components/packet-rain/images/red-packet-rain-open.png";
        // 初始化红包雨
        this.initRainDrops()
        // 音效
        this.audioOfClick()
    },
    audioOfClick() {
//      innerAudioContext.autoplay = false
//      innerAudioContext.src = 'https://www.sunniejs.cn/static/weapp/dianji.mp3'
//      innerAudioContext.onPlay(() => {})
//      innerAudioContext.onError(res => {})
    },
};