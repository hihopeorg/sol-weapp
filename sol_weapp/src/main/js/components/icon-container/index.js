export default{
    /**
     * 组件的属性列表
     */
    externalClasses: ['sol-class'],
    options: {
        multipleSlots: true
    },
    props: {
        // 容器高度
        height: {default:0},
        // 距离顶部高度
        top: {default:80},
        // 距离底部高度
        bottom: {default:100},
        // 回到初始位置
        reset: {default:false},
    },

    data: {
        x: 10,
        y: 0,
        areaH: 0,
        areaBottom: 0,
        toped:0,
        right:0,
    },
    /**
     * 组件的方法列表
     */
    onInit() {
        let areaBottom = this.bottom
        //  计算可以移动的高度
        let areaH = 710 - parseInt(areaBottom) -parseInt(this.top)
        this.areaBottom = areaBottom;
        this.areaH = areaH;
        this.y = areaH - parseInt(this.height);
    },
    moved(e){
        let toped = Math.floor(e.touches[0].globalY)-120;
        if(toped<500 && toped>1){
            this.toped = toped;
        }
    },
    hideIcon(){
        let tiemr2 = setInterval(()=>{
            if(this.right > -50){
                this.right --;
            }else{
                clearInterval(tiemr2);
            }
        },1);
    },
    showIcon(){
        let tiemr = setInterval(()=>{
            if(this.right < 0){
                this.right ++;
            }else{
                clearInterval(tiemr);
            }
        },1);
    },
}
