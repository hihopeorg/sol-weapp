/*
 * @Author: Sunnie 
 * @Date: 2021-09-06 18:42:25 
 * @Last Modified by: Sunnie
 * @Last Modified time: 2021-09-06 18:42:46
 */
export default{
    props: {
        painting: {default:{}},
    },
    data: {
        showCanvas: false,
        width: 100,
        height: 100,
        tempFileList: [],
        isPainting: false
    },
    readyPigment() {
        const { width, height, views } = this.painting;
        this.width= width;
        this.height= height;
        const inter = setInterval(() => {
            if (this) {
                clearInterval(inter)
                this.getImagesInfo(views)
            }
        }, 100)
    },
    group(array, subGroupLength) {
        let index = 0
        let newArray = []
        while (index < array.length) {
            newArray.push(array.slice(index, (index += subGroupLength)))
        }
        return newArray
    },
    getImagesInfo(views) {
        const imageList = []
        for (let i = 0; i < views.length; i++) {
            if (views[i].type === 'image') {
                imageList.push(this.getImageInfo(views[i].url))
            }
        }
        const loadTask = []
        let index = 0
        while (index < imageList.length) {
            loadTask.push(
                new Promise((resolve, reject) => {
                    Promise.all(imageList.slice(index, (index += 8)))
                        .then(res => {
                            resolve(res)
                        })
                        .catch(res => {
                            reject(res)
                        })
                })
            )
        }
        Promise.all(loadTask).then(res => {
            let tempFileList = []
            for (let i = 0; i < res.length; i++) {
                tempFileList = tempFileList.concat(res[i])
            }
            this.tempFileList= tempFileList;
            this.startPainting()
        })
    },
    startPainting() {
        const {
            tempFileList,
            painting: { views }
        } = this
        //  console.log(tempFileList)
        for (let i = 0, imageIndex = 0; i < views.length; i++) {
            if (views[i].type === 'image') {
                this.drawImage({
                    ...views[i],
                    url: tempFileList[imageIndex]
                })
                imageIndex++
            } else if (views[i].type === 'text') {
                if (!this.measureText) {
                    this.triggerEvent('getImage', {
                        errMsg: 'canvasdrawer:version too low'
                    })
                    // 失败重试
                    this.reset()
                    return
                } else {
                    this.drawText(views[i])
                }
            } else if (views[i].type === 'rect') {
                this.drawRect(views[i])
            }
        }
        this.draw(false, () => {
            // 延迟保存图片，解决安卓生成图片错位bug。
            setTimeout(() => {
                this.saveImageToLocal()
            }, 800)
        })
    },
    drawImage(params) {
        this.save()
        const { top = 0, left = 0, width = 0, height = 0,deg = 0 } = params
        // if (borderRadius) {
        //   this.beginPath()
        //   this.arc(left + borderRadius, top + borderRadius, borderRadius, 0, 2 * Math.PI)
        //   this.clip()
        //   this.drawImage(url, left, top, width, height)
        // } else {
        if (deg !== 0) {
            this.translate(left + width / 2, top + height / 2)
            this.rotate((deg * Math.PI) / 180)
            this.drawImage()
        } else {
            this.drawImage()
        }
        // }
        this.restore()
    },
    drawText(params) {
        this.save()
        const {
            MaxLineNumber = 2,
            breakWord = false,
            color = 'black',
            content = '',
            fontSize = 16,
            top = 0,
            left = 0,
            lineHeight = 20,
            textAlign = 'left',
            width,
            bolder = false,
            textDecoration = 'none'
        } = params

        this.beginPath()
        this.setTextBaseline('top')
        this.setTextAlign(textAlign)
        this.setFillStyle(color)
        this.setFontSize(fontSize)

        if (!breakWord) {
            this.fillText(content, left, top)
            this.drawTextLine(left, top, textDecoration, color, fontSize, content)
        } else {
            let fillText = ''
            let fillTop = top
            let lineNum = 1
            for (let i = 0; i < content.length; i++) {
                fillText += [content[i]]
                if (this.measureText(fillText).width > width) {
                    if (lineNum === MaxLineNumber) {
                        if (i !== content.length) {
                            fillText = fillText.substring(0, fillText.length - 1) + '...'
                            this.fillText(fillText, left, fillTop)
                            this.drawTextLine(left, fillTop, textDecoration, color, fontSize, fillText)
                            fillText = ''
                            break
                        }
                    }
                    this.fillText(fillText, left, fillTop)
                    this.drawTextLine(left, fillTop, textDecoration, color, fontSize, fillText)
                    fillText = ''
                    fillTop += lineHeight
                    lineNum++
                }
            }
            this.fillText(fillText, left, fillTop)
            this.drawTextLine(left, fillTop, textDecoration, color, fontSize, fillText)
        }

        this.restore()

        if (bolder) {
            this.drawText({
                ...params,
                left: left + 0.3,
                top: top + 0.3,
                bolder: false,
                textDecoration: 'none'
            })
        }
    },
    drawTextLine(left, top, textDecoration, color, fontSize, content) {
        if (textDecoration === 'underline') {
            this.drawRect({
                background: color,
                top: top + fontSize * 1.2,
                left: left - 1,
                width: this.measureText(content).width + 3,
                height: 1
            })
        } else if (textDecoration === 'line-through') {
            this.drawRect({
                background: color,
                top: top + fontSize * 0.6,
                left: left - 1,
                width: this.measureText(content).width + 3,
                height: 1
            })
        }
    },
    drawRect(params) {
        this.save()
        const { background, top = 0, left = 0, width = 0, height = 0, radius = 0 } = params
        this.setFillStyle(background)
        if (radius) {
            // 开始绘制
            this.beginPath()
            // 左上角
            this.arc(left + radius, top + radius, radius, Math.PI, Math.PI * 1.5)
            // border-top
            this.moveTo(left + radius, top)
            this.lineTo(left + width - radius, top)
            this.lineTo(left + width, top + radius)
            // 右上角
            this.arc(left + width - radius, top + radius, radius, Math.PI * 1.5, Math.PI * 2)
            // border-right
            this.lineTo(left + width, top + height - radius)
            this.lineTo(left + width - radius, top + height)
            // 右下角
            this.arc(left + width - radius, top + height - radius, radius, 0, Math.PI * 0.5)
            // border-bottom
            this.lineTo(left + radius, top + height)
            this.lineTo(left, top + height - radius)
            // 左下角
            this.arc(left + radius, top + height - radius, radius, Math.PI * 0.5, Math.PI)
            // border-left
            this.lineTo(left, top + radius)
            this.lineTo(left + radius, top)
            // 这里是使用 fill 还是 stroke都可以，二选一即可，但是需要与上面对应
            this.fill()
            this.closePath()
        } else {
            this.fillRect(left, top, width, height)
        }
        this.restore()
    },

    getImageInfo(url) {
        if (!url) {
            // 失败重置
            this.reset()
            this.triggerEvent('getImage', {
                errMsg: 'image:image url is none'
            })
        }
        return new Promise((resolve) => {
            if (this.cache[url]) {
                resolve(this.cache[url])
            }
        })
    },
    // 失败后重置
    reset() {
        this.showCanvas= false;
        this.isPainting= false;
        this.tempFileList= [];
    },
}
