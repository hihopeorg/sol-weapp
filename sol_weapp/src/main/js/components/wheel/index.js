export default{
    props: {
        // 划分区域
        areaNumber: {default:6},
        awardNumer: {default:1},
        // 速度
        speed:{default:16},
        // 抽奖模式:1:盘转,2:指针旋转
        mode: {default:2},
        wheelBg: {default:""},
        wheelPointer: {default:""}
    },
    data: {
        deg: 0,
        singleAngle: '', // 每片扇形的角度
        isStart: false
    },
    onInit() {
        let { areaNumber } = this
        const singleAngle = 360 / parseInt(areaNumber)
        this.singleAngle=singleAngle;
        this.$watch("mode","attached")
    },
    // 点击开始抽奖
    start() {
        this.$emit("start");
    },
    begin(e) {
        // 点击开始抽奖
        let XGspeed ;
        let { deg, singleAngle, speed, isStart, mode } = this
        this.singleAngle = 360 / parseInt(this.areaNumber)
        if (isStart) return
        this.isStart = true
        let endAddAngle = 0;
        if (mode == 2) {
            endAddAngle = 360 - ((parseInt(e) - 1) * parseInt(singleAngle) + parseInt(singleAngle) / 2) // 中奖角度
            XGspeed = 100;
        } else {
            endAddAngle = (parseInt(e) - 1) * parseInt(singleAngle) + parseInt(singleAngle) / 2 + 360 // 中奖角度
            XGspeed = 1000/60;
        }
        const rangeAngle = (Math.floor(Math.random() * 4) + 4) * 360 // 随机旋转几圈再停止
        let cAngle
        deg = 0
        this.timer = setInterval(() => {
            if (deg < rangeAngle) {
                deg = deg + parseInt(speed)
            } else {
                cAngle = (endAddAngle + rangeAngle - deg) / parseInt(speed);
                cAngle = cAngle > parseInt(speed) ? speed : cAngle < 1 ? 1 : cAngle;
                deg = deg + parseInt(cAngle)
                if (deg >= endAddAngle + rangeAngle) {
                    deg = endAddAngle + rangeAngle
                    this.isStart = false
                    clearInterval(this.timer)
                    this.$emit("success")
                }
            }
            this.singleAngle= singleAngle;
            this.deg= deg+"deg";
            this.mode= mode;
        }, XGspeed)
    },

    attached() {
        this.deg=0;
    }
}
